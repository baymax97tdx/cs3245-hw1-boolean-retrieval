#!/usr/bin/python3

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import re
import nltk
import sys
import getopt
from collections import defaultdict, namedtuple, Counter
from typing import Iterable, Tuple, Any
from functools import reduce
import itertools
import math
import operator

## configs
GRAM_SIZE = 4
CASE_SENSITIVE = True
LOG_PROB = True
PADDING = True
ADD_ONE_SMOOTHING = True
UNCERTAINTY_TRESHOLD = 0.5
START = "<s>"
STOP = "</s>"
UNKNOWN = "other"

Prediction = namedtuple("Prediction", ["label", "score", "uncertainty_rate"])


class NGramLM:
    def __init__(
        self,
        gram_size: int = GRAM_SIZE,
        case_sensitive: bool = CASE_SENSITIVE,
        log_prob: bool = LOG_PROB,
        padding: bool = PADDING,
        add_one_smoothing: bool = ADD_ONE_SMOOTHING,
        uncertainty_threshold: float = UNCERTAINTY_TRESHOLD,
    ):
        ## model configs
        self.gram_size = gram_size
        self.case_sensitive = case_sensitive
        self.log_prob = log_prob
        self.padding = padding
        self.add_one_smoothing = add_one_smoothing
        self.uncertainty_threshold = uncertainty_threshold

        ## label -> [text] mapping
        self.records = defaultdict(list)
        ## label -> {ngram: count} mapping
        self.label_to_ngram_count_mapping = None
        ## label -> {ngram: probability} mapping
        self.models = None

    def add_record(self, label: str, text: str) -> None:
        """Add a text and its corresponding label to the model"""
        self.records[label].append(text)

    def check(self):
        """Verify the validity of the model"""
        if not self.models or not self.label_to_ngram_count_mapping:
            print("No trained models")
            return

        for label, probability_table in self.models.items():
            print(label, "Total probability:", sum(probability_table.values()))

    def train(self) -> None:
        """Train the model based on previously added records"""

        print("Sample ngrams")
        preview(self.parse_to_ngrams(list(self.records.values())[0][0]), show_tail=True)

        ## compute label -> {ngram: count} mapping WITHOUT smoothing
        self.label_to_ngram_count_mapping = {
            label: Counter(itertools.chain(*map(self.parse_to_ngrams, texts)))
            for label, texts in self.records.items()
        }

        if self.add_one_smoothing:
            ## find observed unique ngrams
            all_ngrams_add_one_count = Counter(
                set(
                    itertools.chain(
                        *map(
                            lambda ngram_count: ngram_count.keys(),
                            self.label_to_ngram_count_mapping.values(),
                        )
                    )
                )
            )

            ## compute label -> {ngram: count} mapping WITH add-one smoothing
            self.label_to_ngram_count_mapping = {
                label: raw_ngram_count + all_ngrams_add_one_count
                for label, raw_ngram_count in self.label_to_ngram_count_mapping.items()
            }

        ## compute label -> {ngram: probability} mapping
        self.models = {}
        for label, ngram_count in self.label_to_ngram_count_mapping.items():
            total_count_for_label = sum(ngram_count.values())

            probability_table = {
                ngram: count / total_count_for_label
                for ngram, count in ngram_count.items()
            }

            self.models[label] = probability_table

    def parse_to_ngrams(self, text: str) -> Iterable[Tuple]:
        """Parse input text to ngrams"""
        if not text:
            return []

        if not self.case_sensitive:
            text = text.lower()

        characters = tuple(text)

        if self.padding:
            num_paddings = self.gram_size - 1
            characters = (START,) * num_paddings + characters + (STOP,) * num_paddings

        ## group characters into ngrams
        ngrams = map(
            lambda i: characters[i : i + self.gram_size],
            range(len(characters) - self.gram_size + 1),
        )

        ## discard invalid ngrams
        valid_ngrams = filter(
            lambda ngram: len(ngram) == self.gram_size
            and (ngram[0] != START or ngram[-1] != STOP),
            ngrams,
        )

        return list(valid_ngrams)

    def predict(self, text) -> str:
        """Predict which language model the input text belongs to"""
        if not self.models:
            print("No trained models")
            return

        def get_predictions() -> Iterable[Prediction]:
            predictions = []
            ## convert input text to ngrams
            ngrams = self.parse_to_ngrams(text)

            for label, probability_table in self.models.items():
                ## lookup probability of each ngram, probability is 0 if ngram has never been seen before
                probabilities = [probability_table.get(ngram, 0) for ngram in ngrams]

                non_zero_probabilities = [
                    probability for probability in probabilities if probability > 0
                ]

                ## number of ngrams that has never seen before / total number of input ngrams
                uncertainty_rate = 1 - len(non_zero_probabilities) / len(probabilities)
                ## calculate score either using product of probabilities or sum of log of probabilities
                score = (
                    sum(map(math.log, non_zero_probabilities))
                    if self.log_prob
                    else reduce(operator.mul, non_zero_probabilities)
                )

                prediction = Prediction(
                    label=label, score=score, uncertainty_rate=uncertainty_rate
                )

                predictions.append(prediction)

            ## only select predictions which has an uncertainty rate < threshold
            valid_predictions = [
                prediction
                for prediction in predictions
                if prediction.uncertainty_rate < self.uncertainty_threshold
            ]

            return valid_predictions

        predictions = get_predictions()
        # preview(predictions)
        best_prediction = max(
            predictions, default=None, key=lambda prediction: prediction.score
        )

        return best_prediction.label if best_prediction is not None else UNKNOWN


def preview(
    entries: Iterable[Any], num_preview_entries: int = 5, show_tail: bool = False
):
    """Preview any iterable collection"""
    num_entries = len(entries)
    print("Total number of entries:", num_entries)

    print("Preview:")
    print("-" * 50)
    ## show head entries
    for entry in entries[:num_preview_entries]:
        print(entry)

    num_hidden_entries = max(
        num_entries - (num_preview_entries * 2 if show_tail else num_preview_entries), 0
    )
    if num_hidden_entries > 0:
        print(f"...{num_hidden_entries} more entries")

    offset = num_preview_entries + num_hidden_entries
    ## show tail entries
    for entry in entries[offset : offset + num_preview_entries]:
        print(entry)

    print("-" * 50)


def get_label_text(line: str):
    """Parse line with the following format: '<label> <text>'"""
    return line.split(" ", 1)


def build_LM(in_file: str):
    """
    Build language models for each label
    each line in in_file contains a label and a string separated by a space
    """
    print("Building language models...\n")

    model = NGramLM()

    print("Reading training data...\n")
    with open(in_file,"r",encoding ="utf8") as f:
        lines = [line.strip("\r\n") for line in f.readlines()]

        preview(lines)

        for line in lines:
            label, text = get_label_text(line)
            model.add_record(label.lower(), text)

    print("Training models...\n")
    model.train()
    print("Training done!\n")

    print("Checking models...\n")
    model.check()
    print()

    return model


def test_LM(in_file: str, out_file: str, LM: NGramLM):
    """
    Test the language models on new strings
    each line of in_file contains a string
    you should print the most probable label for each string into out_file
    """
    print("Testing language models...\n")

    print("Reading test data...\n")
    with open(in_file, "r") as in_f, open(out_file, "w") as out_f:
        lines = [line.strip("\r\n") for line in in_f.readlines()]

        preview(lines)

        print("Predicting test data...\n")
        for line in lines:
            print(LM.predict(line), line, file=out_f)

    print("Prediction done!")


def usage():
    print(
        "usage: "
        + sys.argv[0]
        + " -b input-file-for-building-LM -t input-file-for-testing-LM -o output-file"
    )


input_file_b = input_file_t = output_file = None
try:
    opts, args = getopt.getopt(sys.argv[1:], "b:t:o:")
except getopt.GetoptError:
    usage()
    sys.exit(2)
for o, a in opts:
    if o == "-b":
        input_file_b = a
    elif o == "-t":
        input_file_t = a
    elif o == "-o":
        output_file = a
    else:
        assert False, "unhandled option"
if input_file_b == None or input_file_t == None or output_file == None:
    usage()
    sys.exit(2)

LM = build_LM(input_file_b)
test_LM(input_file_t, output_file, LM)
