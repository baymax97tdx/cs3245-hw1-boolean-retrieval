This is the README file for A0187836L's submission
email: e0323420@u.nus.edu
== Python Version ==

I'm (We're) using Python Version 3.8.3 for
this assignment.

== General Notes about this assignment ==

In this assignment, I created a class called NGgram which is used to represent our model. 
Ngram takes in 3 arguments: 
    -case_sensitive: A boolean to see if we would like to change all character to lower case
    -padding: A boolean to see if padding is added 
    -scoring_method: Type of method used to calculate probability

In our Ngram Class, I have defined 5 methods:
    - summary: It outputs the total sum of probabilites for each label and also the number of distinct tokens.
               The purpose of summary is to ensure that training of the model has been conducted correctly.
    - add: This method takes in a text input and has no output as its purpose is just to add new records to self.vocab_space to store the training texts according to their labels
    - convert_text_to_ngram: It takes in a text input and outputs a list of the character tokens. 
    - train: This method has no output and its purpose is to prepare the probabilities required for us to calculate if a certain text belongs to a certain label.
    - predict: This method takes in a text and predicts a label out.

The flow of this algorithm is straightforward. We first store all of the training text into our Ngram Object first. Afterwards, the model converts all of the texts
into character tokens. The model then calculates the respective counts and implements smoothing. It then calculates the probability of each token occuring. 
For the prediction part, it will take in a text and convert it into character tokens. We will then calculate its uncertainty rate,probability score and 
output the label accordingly. 

There are a few noteworthy parts of my algorithm. Firstly, we have booleans that allows for either padding or no padding, case sensitive or no case sensitive . 
This is to allow user to customize and easily see which combination of metrics will yield the best result. 
After testing, padding and having case sensitive helps to yield the best result, holding all other variables constant. I believe that 
case sensitive is particularly important because it shows where the specific word is used in the sentence and can help further differentiate 
which language it belongs to.

Next, We have a uncertainty threshold constant. This is because in the test text, there might be unseen words and thus if we include the probability of having
seen these words, it will result in 0. Hence, an uncertainty threshold is set such that even if there is unforeseen words in the test text,
if the overall percentage uncertainty is less than the threshold we set, we simply ignore these characters and focus on the seen ones. If the overall
percentage uncertainty is greater than our threshold, we will not consider that label as a possible output. With the current test set, we require a minimum threshold
of 0.38 to correctly classify all test inputs. However, since test set is very small. I decided to leave the threshold at 0.50. This means if over half
of the tokens are unknown, we remove that label from being considered.

Lastly, We have different methods to calculate probability. The 3 methods avaiable are labelled "log","constant" and "multi".
"multi" represents the conventional method of just multiplying the probabilities up. However, when using this approach, accuracy is only 70%. 
This could be attributed to computational error because the probabilities are generally small numbers (e^-6 or smaller) and thus when the 
computer perform this multiplications, there is accuracy lost in the value calculated.
In order to solve this, there are two other methods implemented to try and improve. The first is "log".
The idea is to log the probabilities and sum them up together. The log of the original number will be much bigger  and 
there is less error in computation because summation is a much better alternative compared to multiplication. When using this method, accuracy is 100%.
The last method is 'constant' which multiplies a large constant to each probability before multiplying the probabilities up together. The purpose 
of this method is try to make the numbers bigger and so that when multiplcation occurs, there will not be huge accuracy lost. When applying this method, 
we are also able to yield 100% accuracy. However, the most ideal method should still be "log" since we are able to avoid multiplcation.   

Overall, with the current training text file and test file, these are the ideal parameters:
case_sensitive= True,
padding = True,
scoring_method = "log" 


== Files included with this submission ==

- build_test_LM.py (main python script to run code)
- README.txt 

== Statement of individual work ==

Please put a "x" (without the double quotes) into the bracket of the appropriate statement.

[x] I, A0187836L, certify that I have followed the CS 3245 Information
Retrieval class guidelines for homework assignments.  In particular, I
expressly vow that I have followed the Facebook rule in discussing
with others in doing the assignment and did not take notes (digital or
printed) from the discussions.  

[ ] I, A0000000X, did not follow the class rules regarding homework
assignment, because of the following reason:

<Please fill in>

I suggest that I should be graded as follows:

<Please fill in>

== References ==
Tan Kai Qun Jeremy e0272509@u.nus.edu

<Please list any websites and/or people you consulted with for this
assignment and state their role>
