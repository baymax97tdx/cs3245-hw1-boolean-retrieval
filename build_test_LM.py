#!/usr/bin/python3

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import re
import nltk
import sys
import getopt
from collections import defaultdict,Counter
from itertools import chain
from functools import reduce
import math

class NGram:
    def __init__(
        self, 
        case_sensitive, #boolean
        padding, #boolean
        uncertainty_rate, #int
        scoring_method #string
    ):
        # This are the the model parameters that can be tweaked
        self.gram_size=4
        self.case_sensitive=case_sensitive
        self.padding=padding 
        self.uncertainty_level=uncertainty_rate
        self.scoring_method=scoring_method

        # dictionary to contain mapping (label:text) 
        self.vocab_space=defaultdict(list)
        # dictionary to contain ngram count (label=> {ngram:count})
        self.ngram_count={} 
        # dictionary to contain ngram probability (label => {ngram:probability})
        self.ngram_prob={}
    
    def summary(self):
        """Check the contents of the model and see the distribution"""
        if len(self.ngram_prob)==0:
            print("No Models Trained")
        
        for label,probability in self.ngram_prob.items():
            print(label,"total probability is:",sum(probability.values()))
            print("Number of character tokens in",label,"is",len(probability),"\n")

    def add(self,label,text):
        """Adds the text and  label to the vocab space"""
        if not self.case_sensitive:
            text=text.lower()
        if label in self.vocab_space:
            self.vocab_space[label].append(text)
        else:
            self.vocab_space[label]=[text,] 
    
    def convert_text_to_ngram(self,text):
        """Converts the sentence into the Ngram characters
            Returns a list of the segmented text(tuples)"""
        #Convert text into string of characters, tuple immutable so fits better than list
        characters = tuple(text)

        #For padding, if Ngram is 4, Then starting will have to be START START START a
        #Formula for padding = gram_size - 1 
        #Pad first before segmentating the text 
        if self.padding:
            padding = self.gram_size-1
            start  = 'START' 
            end = 'END'
            characters = (start,) * padding + characters + (end,) * padding 

        # Group the characters into ngram 
        ngram = [] 
        for i in range(0,len(characters)-self.gram_size+1):
            ngram.append(characters[i:i+self.gram_size])
        
        # Check to see if segmentation has any weird error
        final_ngram = [] 
        front_check = True
        back_check = True 
        if ngram[0][0] != start:
            print ('Start padding is wrong')
            front_check = False
        if ngram[-1][-1] != end:
            print ('End padding is wrong')
            back_check = False 
        if front_check and back_check:
            for i in ngram:
                if len(i) != self.gram_size:
                    print (i,' is invalid size') 
                else:
                    final_ngram.append(i)
        return final_ngram 

    def train(self):
        """Train model with the help of the training dataset
            Step 1: Get the Counts of the observations and sort into respective labels
            Step 2: Add Smoothing
            Step 3: Calculate Probability"""

        # Step 1 
        """apply map and converting each text into a list of 4 gram texts """
        self.ngram_count = {label:Counter(chain(*map(self.convert_text_to_ngram,text))) 
                            for label,text in self.vocab_space.items()}
                               
        # Step 2
        """Find all unique ngrams from ngram_count and create another Counter"""
        smoothing_counter = Counter(set(chain(*self.ngram_count.values())))

        """Add the Counters from Step 1 and Step 2 """
        self.ngram_count = {label:original_ngram_count+smoothing_counter
                            for label,original_ngram_count in self.ngram_count.items()}

         # Step 3 
        """Find total number of observations then find the probability of each ngram"""
        for label,total_ngram in self.ngram_count.items():
            total_frequency = sum(total_ngram.values())
            probability_score = {ngram:count/total_frequency for ngram,count in total_ngram.items()}
            self.ngram_prob[label]=probability_score

    def predict(self,text):
        if not self.ngram_prob:
            print ('Model not trained')
            return
        if not self.case_sensitive:
            text=text.lower()
        prediction = [] 
        # Convert input text into ngram
        ngram_input = self.convert_text_to_ngram(text)

        for label, probability in self.ngram_prob.items():
            # Find probability of each input ngram, 0 if not found
            probabilities = [probability.get(ngram,0) for ngram in ngram_input]

            # Find number of non zero probability to see how many ngrams are actually found
            non_zero_probability = [p for p in probabilities if p>0]

            # Check what is the proportion of 0 probability in the ngram 
            unknown_rate = 1 - len(non_zero_probability)/len(ngram_input)

            # If unknown rate higher or equal to threshold set, dont include this label into our consideration.
            if unknown_rate > self.uncertainty_level:
                continue

            # Get the probability score from the non zero probabilities
            if self.scoring_method =='log': 
                score  = reduce(lambda x,y: x+y,map(math.log,non_zero_probability))
            
            elif self.scoring_method == 'multi' or self.scoring_method == 'constant':
                if self.scoring_method=='constant':
                    non_zero_probability = map(lambda x:x*100000,non_zero_probability)
                score = reduce(lambda x,y:x*y,non_zero_probability)

            # Store the label, score and unknown rate as a tuple in prediction 
            # Index representation  0 - label , 1 - score 
            prediction.append((label,score))
            
        # Get the most suited label based on the probability score

        label_output = max(prediction,default=None,key=lambda x:x[1])

        return label_output[0] if label_output is not None else 'other'



def build_LM(in_file):
    """
    build language models for each label
    each line in in_file contains a label and a string separated by a space
    """
    print("building language models...\n")
    # Initiate Instance
    model = NGram(True,True,0.5,'log')
    # Read Training Data
    print("Reading Training Input\n")
    with open(in_file,"r",encoding ="utf8") as f:
        # Using Windows Laptop hence \r\n
        lines = [text.strip("\r\n") for text in f.readlines()]

        # Separate label and the text Sentence
        for line in lines:
            label,text = line.split(" ",1)
            model.add(label.lower(),text)
            
    print("Training Data read\n")


    print ("Training Model Now\n")

    model.train()

    print ("Training finished\n")
    #Ensure model has been trained
    model.summary()

    return model 




def test_LM(in_file, out_file, LM):
    """
    test the language models on new strings
    each line of in_file contains a string
    you should print the most probable label for each string into out_file
    """
    print("\n")
    print("Initating Test \n")
    
    print("Reading Test input\n")
    with open(in_file, "r") as in_f, open(out_file, "w") as out_f:
         lines = [text.strip("\r\n") for text in in_f.readlines()]

         for line in lines:
             print(LM.predict(line),line,file=out_f)
    
    print ('Done')



def usage():
    print(
        "usage: "
        + sys.argv[0]
        + " -b input-file-for-building-LM -t input-file-for-testing-LM -o output-file"
    )


input_file_b = input_file_t = output_file = None
try:
    opts, args = getopt.getopt(sys.argv[1:], "b:t:o:")
except getopt.GetoptError:
    usage()
    sys.exit(2)
for o, a in opts:
    if o == "-b":
        input_file_b = a
    elif o == "-t":
        input_file_t = a
    elif o == "-o":
        output_file = a
    else:
        assert False, "unhandled option"
if input_file_b == None or input_file_t == None or output_file == None:
    usage()
    sys.exit(2)

LM = build_LM(input_file_b)
test_LM(input_file_t, output_file, LM)
